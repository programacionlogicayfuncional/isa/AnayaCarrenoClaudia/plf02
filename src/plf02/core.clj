(ns plf02.core)

(defn función-associative?-1
  [xs]
  (associative? xs))

(defn función-associative?-2
  [xs]
  (associative? xs))

(defn función-associative?-3
  [xs]
  (associative? xs))

(función-associative?-1 [10 20 30])
(función-associative?-2 (map (fn [xs] (* 2 xs)) [10 20 30 40]))
(función-associative?-3 #{:uno 1 :dos 2 :tres 3})

(defn función-boolean?-1
  [x]
  (boolean? x))

(defn función-boolean?-2
  [x]
  (boolean? x))

(defn función-boolean?-3
  [x]
  (boolean? x))

(función-boolean?-1 (first ["hola" false 1 false]))
(función-boolean?-2 (true? (+ 5 -2)))
(función-boolean?-3 (keys {:uno 1 :dos 2}))

(defn función-char?-1
  [x]
  (char? x))

(defn función-char?-2
  [x]
  (char? x))

(defn función-char?-3
  [x]
  (char? x))

(función-char?-2 (find {:1 1 :2 2 :3 2} :2))
(función-char?-1 "hola")
(función-char?-3 (first [\a \b 1 6 70]))

(defn función-coll?-1
  [cs]
  (coll? cs))

(defn función-coll?-2
  [cs]
  (coll? cs))

(defn función-coll?-3
  [cs]
  (coll? cs))

(función-coll?-1 (concat [7] [1 2 3]))
(función-coll?-2 ((fn [x] [x (+ 2 x)]) 10))
(función-coll?-3 (boolean (* 10 8)))

(defn función-decimal?-1
  [n]
  (decimal? n))

(defn función-decimal?-2
  [n]
  (decimal? n))

(defn función-decimal?-3
  [n]
  (decimal? n))

(función-decimal?-1 50/20)
(función-decimal?-2 (* 12.5986784 0.000000025896))
(función-decimal?-3 1M)

(defn función-double?-1
  [n]
  (double? n))

(defn función-double?-2
  [n]
  (double? n))

(defn función-double?-3
  [n]
  (double? n))

(función-double?-1 (((fn [a] (fn [b] (* a b))) 1000000) 0.18963))
(función-double?-2 (first (map + #{1 2 3 4 5})))
(función-double?-3 "hola")

(defn función-float?-1
  [n]
  (float? n))

(defn función-float?-2
  [n]
  (float? n))

(defn función-float?-3
  [n]
  (float? n))

(función-float?-1 (/ 10 4.5))
(función-float?-2 (apply max [3 5 2.5 7.89]))
(función-float?-3 (min 1 0 4 )) 

(defn función-ident?-1
  [x]
  (ident? x))

(defn función-ident?-2
  [x]
  (ident? x))

(defn función-ident?-3
  [x]
(ident? x))

(función-ident?-1 (first( keys {:uno 1 :dos 2 :tres 3})))
(función-ident?-2 (first [1 2 3 4 5 6]))
(función-ident?-3 "hola")

(defn función-indexed?-1
  [cs]
  (indexed? cs))

(defn función-indexed?-2
  [cs]
  (indexed? cs))

(defn función-indexed?-3
  [cs]
  (indexed? cs))

(función-indexed?-1 1) 
(función-indexed?-2 [1 2 3 4]) 
(función-indexed?-3 {:uno 1 :dos 2 :tres 3})

(defn función-int?-1
  [x]
  (int? x))

(defn función-int?-2
  [x]
  (int? x))

(defn función-int?-3
  [x]
  (int? x))

(función-int?-1 1)
(función-int?-2 (first (range 10.5 30 0.5)))
(función-int?-3 (sequence [1 3 6 8 0]))

(defn función-integer?-1
  [x]
  (integer? x))

(defn función-integer?-2
  [x]
  (integer? x))

(defn función-integer?-3
  [x]
  (integer? x))

(función-integer?-1 (* 1N 2))
(función-integer?-2  (rand-int -100)) 
(función-integer?-3 (inc 10))

(defn función-keyword?-1
  [x]
  (keyword? x))

(defn función-keyword?-2
  [x]
  (keyword? x))

(defn función-keyword?-3
  [x]
  (keyword? x))

(función-keyword?-1 :3 )
(función-keyword?-2 (keys #{12 23 45}))
(función-keyword?-3 (first {:1 1 :2 4 :3 6 :4 7 :5 9}))

(defn función-list?-1
  [x]
  (list? x))

(defn función-list?-2
  [x]
  (list? x))

(defn función-list?-3
  [x]
  (list? x))

(función-list?-1 (list 10 20 30))
(función-list?-2 (empty '(1 2 3 4 5)))
(función-list?-3 (count '(1 2 3 4 5)))

(defn función-map-entry?-1
  [x]
  (map-entry? x))

(defn función-map-entry?-2
  [x]
  (map-entry? x))

(defn función-map-entry?-3
  [x]
  (map-entry? x))

(función-map-entry?-1 (map-entry? (first #{1 2})))
(función-map-entry?-2 (map-entry? :10))
(función-map-entry?-3 (map-entry? (first {:a 1 :b 2})))

(defn función-map?-1
  [x]
  (map? x))

(defn función-map?-2
  [x]
  (map? x))

(defn función-map?-3
  [x]
  (map? x))

(función-map?-1 (map (fn [a] (* 2 a)) [1 2 3 4 5]))
(función-map?-2 [15 8 9 20]) 
(función-map?-3 (first [{1 12 2 21 3 31 4 41} 8 [5]])) 

(defn función-nat-int?-1
  [x]
  (nat-int? x))

(defn función-nat-int?-2
  [x]
  (nat-int? x))

(defn función-nat-int?-3
  [x]
  (nat-int? x))

(función-nat-int?-1 (apply < [10 8 20 6 30])) 
(función-nat-int?-2 100)
(función-nat-int?-3 (- 3 5)) 

(defn función-number?-1
  [x]
  (number? x))

(defn función-number?-2
  [x]
  (number? x))

(defn función-number?-3
  [x]
  (number? x))

(función-number?-1 (count #{1 2 3 4 5 6 7 8 9}))
(función-number?-2 "hi") 
(función-number?-3 (even? 3)) 

(defn función-pos-int?-1
  [x]
  (pos-int? x))

(defn función-pos-int?-2
  [x]
  (pos-int? x))

(defn función-pos-int?-3
  [x]
  (pos-int? x))

(función-pos-int?-1 (- 4 80))
(función-pos-int?-2 (first [10 20 -10 -20]))
(función-pos-int?-3 5/7)

(defn función-ratio?-1
  [x]
  (ratio? x))

(defn función-ratio?-2
  [x]
  (ratio? x))

(defn función-ratio?-3
  [x]
  (ratio? x))

(función-ratio?-1 4/14) 
(función-ratio?-2 (first [7/2 0.5 67]))
(función-ratio?-3 (((fn [a] (fn [b] (* a b))) 3/6) 6))

(defn función-rational?-1
  [x]
  (rational? x))

(defn función-rational?-2
  [x]
  (rational? x))

(defn función-rational?-3
  [x]
  (rational? x))

(función-rational?-1 22/7)
(función-rational?-2 (/ 6 8)) 
(función-rational?-3 (* 3/4 16))

(defn función-seq?-1
  [cs]
  (seq? cs))

(defn función-seq?-2
  [cs]
  (seq? cs))

(defn función-seq?-3
  [cs]
  (seq? cs))

(función-seq?-1 (map [3 4 5 6]))
(función-seq?-2 (seq {:1 "uno" :2 "dos" }))
(función-seq?-3 (list "a" "b" "c" "d")) 

(defn función-seqable?-1
  [x]
  (seqable? x))

(defn función-seqable?-2
  [x]
  (seqable? x))

(defn función-seqable?-3
  [x]
  (seqable? x))

(función-seqable?-1  {:uno 1 :dos 2 :tres 3})
(función-seqable?-2 "Holaaaa")
(función-seqable?-3 4)

(defn función-sequential?-1
  [cs]
  (sequential? cs))

(defn función-sequential?-2
  [cs]
  (sequential? cs))

(defn función-sequential?-3
  [cs]
  (sequential? cs))

(función-sequential?-1 [1 2 3 4 5]) 
(función-sequential?-2 (range 10 20 0.5))
(función-sequential?-3 [{:a 1 :b 2} [\a \b \c] #{1 2}]) 

(defn función-set?-1
  [x]
  (set? x))

(defn función-set?-2
  [x]
  (set? x))

(defn función-set?-3
  [x]
  (set? x))

(función-set?-1 (hash-set))
(función-set?-2 {1 1 2 2 3 3})
(función-set?-3 (set [1 2 3]))

(defn función-some?-1
  [x]
  (some? x))

(defn función-some?-2
  [x]
  (some? x))

(defn función-some?-3
  [x]
  (some? x))

(función-some?-1 (hash-set 1 2 3 4 5)) 
(función-some?-2 nil)
(función-some?-3 [])

(defn función-string?-1
  [x]
  (string? x))

(defn función-string?-2
  [x]
  (string? x))

(defn función-string?-3
  [x]
  (string? x))

(función-string?-1 (first (into [] (range 10))))
(función-string?-2 (first (vals {:1 "marcos" :2 "sergio" :3 "katy" :4 "clau"})))
(función-string?-3 "holaaa")

(defn función-symbol?-1
  [x]
  (symbol? x))

(defn función-symbol?-2
  [x]
  (symbol? x))

(defn función-symbol?-3
  [x]
  (symbol? x))

(función-symbol?-1  'a)
(función-symbol?-2 "a")
(función-symbol?-3 '?) 

(defn función-vector?-1
  [vs]
  (vector? vs))

(defn función-vector?-2
  [vs]
  (vector? vs))

(defn función-vector?-3
  [vs]
  (vector? vs))

(función-vector?-1 [1 3 5 8])
(función-vector?-2 (list 1 3 5 8))
(función-vector?-3 ["hi" "bye"])

;; Funciones de Orden Superior

(defn función-drop-1
  [n]
  (drop n))

(defn función-drop-2
  [n cs]
  (drop n cs))

(defn función-drop-3
  [n cs]
  (drop n cs))

(función-drop-1 5)
(función-drop-2 1 [10 20 30 40 50])
(función-drop-3 2 #{ 60 70 80})

(defn función-drop-last-1
  [cs]
  (drop-last cs))

(defn función-drop-last-2
  [n cs]
  (drop-last n cs))

(defn función-drop-last-3
  [n cs]
  (drop-last n cs))

(función-drop-last-1 [10 20 30 40]) 
(función-drop-last-2 5 [1 2 3 4 5 6 7 8 9 0])
(función-drop-last-3 2 #{60 70 80 90 100})

(defn función-drop-while-1
  [pred]
  (drop-while pred))

(defn función-drop-while-2
  [pred cs]
  (drop-while pred cs))

(defn función-drop-while-3
  [pred cs]
  (drop-while pred cs))

(función-drop-while-1 <)
(función-drop-while-2 vector {:1 1 :2 2 :3 3})
(función-drop-while-3 neg? [-5 -6 0 1])

(defn función-every?-1
  [pred cs]
  (every? pred cs))

(defn función-every?-2
  [pred cs]
  (every? pred cs))

(defn función-every?-3
  [pred cs]
  (every? pred cs))

(función-every?-1 + [1 2]) 
(función-every?-2 #{1 2} #{1})
(función-every?-3 #{1 2} #{1 5})

(defn función-filterv-1
  [pred cs]
  (filterv pred cs))

(defn función-filterv-2
  [pred cs]
  (filterv pred cs))

(defn función-filterv-3
  [pred cs]
  (filterv pred cs))

(función-filterv-1 map? (list 1 2 3))
(función-filterv-2 even? [1 2 3 4 5])
(función-filterv-3  empty? {1 1 2 2 3 3 4 4})

(defn función-group-by-1
  [f cs]
  (group-by f cs))

(defn función-group-by-2
  [f cs]
  (group-by f cs))

(defn función-group-by-3
  [f cs]
  (group-by f cs))

(función-group-by-1 count ["a" "hola" "" "adios" "bye" "hello" "b"])
(función-group-by-2 count {:1 "a" :2 "bc" :3 "def"})
(función-group-by-3 pos? [10 20 30])

(defn función-iterate-1
  [f x]
  (iterate f x))

(defn función-iterate-2
  [f x]
  (iterate f x))

(defn función-iterate-3
  [f x]
  (iterate f x))

(función-iterate-1 inc 2)
(función-iterate-2 vector 10) 
(función-iterate-3 list 1)

(defn función-keep-1
  [f cs]
  (keep f cs))

(defn función-keep-2
  [f cs]
  (keep f cs))

(defn función-keep-3
  [f]
  (keep f))

(función-keep-1 #{0 1 2 3 5} #{2 3 4 5})
(función-keep-2 neg? [1 -2 3 -6])
(función-keep-3 (range 10))

(defn función-keep-indexed-1
  [f ]
  (keep-indexed f))

(defn función-keep-indexed-2
  [f cs]
  (keep-indexed f cs))

(defn función-keep-indexed-3
  [f cs]
  (keep-indexed f cs))

(función-keep-indexed-1 < )
(función-keep-indexed-2 vector [1 2 3])
(función-keep-indexed-3 [1 2 3 4] {})

(defn función-map-indexed-1
  [f cs]
  (map-indexed f cs))

(defn función-map-indexed-2
  [f cs]
  (map-indexed f cs))

(defn función-map-indexed-3
  [f cs]
  (map-indexed f cs))

(función-map-indexed-1 vector "clau")
(función-map-indexed-2 list "clau")
(función-map-indexed-3 hash-map "clau") 

(defn función-mapcat-1
  [f]
  (mapcat f))

(defn función-mapcat-2
  [f cs]
  (mapcat f cs))

(defn función-mapcat-3
  [f cs bs]
  (mapcat f cs bs))

(función-mapcat-1 vector)
(función-mapcat-2 list [1 2 3])
(función-mapcat-3 list [:a :b :c] [1 2 3])

(defn función-mapv-1
  [f cs]
  (mapv f cs))

(defn función-mapv-2
  [f c1 c2 ]
  (mapv f c1 c2))

(defn función-mapv-3
  [f c1 c2 c3]
  (mapv f c1 c2 c3))

(función-mapv-1 dec [10 20 30])
(función-mapv-2 * [1 2 3] [4 5 6])
(función-mapv-3  vector [1 2 3] [4 5 6] [7 8 9])

(defn función-merge-with-1
  [f as]
  (merge-with f as))

(defn función-merge-with-2
  [f as bs]
  (merge-with f as bs))

(defn función-merge-with-3
  [f as bs cs]
  (merge-with f as bs cs))

(función-merge-with-1 + {:a 1  :b 2})
(función-merge-with-2 * {:a 1 :b 2 :c 3} {:a 1 :b 2 :c 3})
(función-merge-with-3 * {:a 1 :b 2 :c 3} {:a 1 :b 2 :c 3} {:a 1 :b 2 :c 3})

(defn función-not-any?-1
  [pred cs]
  (not-any? pred cs))

(defn función-not-any?-2
  [pred cs]
  (not-any? pred cs))

(defn función-not-any?-3
  [pred cs]
  (not-any? pred cs))

(función-not-any?-1 nil? [true false false])
(función-not-any?-2 neg? [1 2 3 4])
(función-not-any?-3 * [1 2 3 4])

(defn función-not-every?-1
  [pred cs]
  (not-every? pred cs))

(defn función-not-every?-2
  [pred cs]
  (not-every? pred cs))

(defn función-not-every?-3
  [pred cs]
  (not-every? pred cs))

(función-not-every?-1 odd? '(1 2 3 4) )
(función-not-every?-2 neg? [1 2 3]) 
(función-not-every?-3 even? #{1 2 3}) 

(defn función-partition-by-1
  [f]
  (partition-by f))

(defn función-partition-by-2
  [f as]
  (partition-by f as))

(defn función-partition-by-3
  [f as]
  (partition-by f as))

(función-partition-by-1 +)
(función-partition-by-2 even? [1 1 1 2 2 3 3])
(función-partition-by-3 pos? [1 2 3 4])

(defn función-reduce-kv-1
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-2
  [a b c]
  (reduce-kv a b c))

(defn función-reduce-kv-3
  [a b c]
  (reduce-kv a b c))

(función-reduce-kv-1 concat #{1 2} [])
(función-reduce-kv-2 conj [] [2 4 [78] [9 0]])
(función-reduce-kv-3 concat #{} [])

(defn función-remove-1
  [pred cs]
  (remove pred cs))

(defn función-remove-2
  [pred cs]
  (remove pred cs))

(defn función-remove-3
  [pred cs]
  (remove pred cs))

(función-remove-1 pos? [1 -2 2 -1 3 7 0])
(función-remove-2 neg? [-2 4 -7 8 -9])
(función-remove-3 {:a 42 :b 69} #{:a :b :c} )

(defn función-reverse-1
  [s]
  (reverse s))

(defn función-reverse-2
  [s]
  (reverse s))

(defn función-reverse-3
  [s]
  (reverse s))

(función-reverse-1  "holaa")
(función-reverse-2 [1 2 3 4]) 
(función-reverse-3 {:1 :2 :3 :4})

(defn función-some-1
  [pred cs]
  (some pred cs))

(defn función-some-2
  [pred cs]
  (some pred cs))

(defn función-some-3
  [pred cs]
  (some pred cs))

(función-some-1 even? [1 2 3]) 
(función-some-2 list? {1 2 3 4}) 
(función-some-3 {2 "uno" 3 "clau"} [2])

(defn función-sort-by-1
  [k cs]
  (sort-by k cs))

(defn función-sort-by-2
  [k cs]
  (sort-by k cs))

(defn función-sort-by-3
  [k c cs ]
  (sort-by k c cs))

(función-sort-by-1 count ["oscar" "clau" "katy"])
(función-sort-by-2 first [[10] [2] [33]])
(función-sort-by-3 first > [[10] [2] [33]])

(defn función-split-with-1
  [pred cs ]
  (split-with pred cs))

(defn función-split-with-2
  [pred cs]
  (split-with pred cs))

(defn función-split-with-3
  [pred cs]
  (split-with pred cs))

(función-split-with-1 #{:c} [:a :b :c :d])
(función-split-with-2 odd? [1 3 5 6 7 9])
(función-split-with-3 neg? [1 3 5 6 7 9] )

(defn función-take-1
  [n]
  (take n))

(defn función-take-2
  [n cs]
  (take n cs))

(defn función-take-3
  [n cs]
  (take n cs))

(función-take-1 3)
(función-take-2 3 [3 6 9 0 12 45])
(función-take-3 3 (list 3 6 9 0 12 45))

(defn función-take-last-1
  [n cs]
  (take-last n cs))

(defn función-take-last-2
  [n cs]
  (take-last n cs))

(defn función-take-last-3
  [n cs]
  (take-last n cs))

(función-take-last-1 0 [1 2 3])
(función-take-last-2 3 #{ 1 2 3 4 5 6}) 
(función-take-last-3 2 (list 1 2 3 4 5))

(defn función-take-nth-1
  [n]
  (take-nth n))

(defn función-take-nth-2
  [n cs]
  (take-nth n cs))

(defn función-take-nth-3
  [n cs]
  (take-nth n cs))

(función-take-nth-1 5)
(función-take-nth-2 5 (range 1 20 8))
(función-take-nth-3 2 (take-last 3 #{1 2 3 4 5 6}))

(defn función-take-while-nth-1
  [pred]
  (take-while pred))

(defn función-take-while-nth-2
  [pred cs]
  (take-while pred cs))

(defn función-take-while-nth-3
  [pred cs]
  (take-while pred cs))

(función-take-while-nth-1 neg?)
(función-take-while-nth-2 neg? [-7 -9 1 0 8 ]) 
(función-take-while-nth-3 #{[1 2] [3 4]} #{[1 2]})

(defn función-update-1
  [m k f]
  (update m k f))

(defn función-update-2
  [m k f x]
  (update m k f x))

(defn función-update-3
  [m k f x ]
  (update m k f x ))

(función-update-1 {:1 1 :2 2} :2 dec)
(función-update-2 {:1 1 :2 2} :1 * 10)
(función-update-3 {:1 1 :2 2} :2 - 5)

(defn función-update-in-1
  [m ks f]
  (update-in m ks f))

(defn función-update-in-2
  [m ks f g]
  (update-in m ks f g))

(defn función-update-in-3
  [m ks f g h]
  (update-in m ks f g h))

(función-update-in-1 {:1 1 :2 2} [:2] dec)
(función-update-in-2 {:1 1 :2 2} [:2] + 5)
(función-update-in-3 {:a 3} [:a] / 4 5)
